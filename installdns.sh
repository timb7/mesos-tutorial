#!/bin/sh 
# Script to install mesos-dns

mkdir ~/go && echo created ~/go
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin
go get github.com/tools/godep && echo echo downloaded godep source
go get github.com/mesosphere/mesos-dns && echo downloaded mesos-dns source
cd $GOPATH/src/github.com/mesosphere/mesos-dns
godep go build . && echo mesos-dns built
cp /vagrant/config.json . && echo copied in config.json
